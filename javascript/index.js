var net = require("net");
var JSONStream = require('JSONStream');
var fs = require('fs'); //Fille System library

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var logs = ""; //logs of recieved and sent data
var tick = 0; //game time counter, wich computes the recieved number of data file.
var dir = "Left"; //direction of the car. By default is left (lane 0)
var once = true; //variable for making actions just once
var switchTick = []; //array of switching lanes
var nextSwitchPointer = 0; //Uses for pointing on next switch

// Funciton for storing/saving information in files
var storeInfo = function(location, info){ 
  fs.writeFile(location, info, function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log("The file was saved!");
    }
  }); 
}

//Send Switch lanes signal
var sendSwitchLane = function(dir){
  send({
    msgType: "switchLane",
    data: dir
  })
}

//Send change throttle
var sendThrottle = function(throttle){
  send({
    msgType: "throttle",
    data: throttle
  });
}

/*
  Funcioin of creating an array(switchTick) with the places on road
  where should the car Switch the Lane.
  Every element of array consist of 
  "dir" - which direciton to change the lane (Right or Left),
  "pieceIndex" - on which piece of road to switch.

  It finds the pieces where it is possible to SwitchLane ("switchPlace1" and "switchPlace2")
  and compute the sum of the angle. Based on "sumAngle" it make the decision where to swicth the lane.
  "switchPlace0" is a variable to save an older switchPlace1.
*/
var createSwitchTick = function(route){
  var switchPlace0 = 0; //switch place variables
  var switchPlace1 = -1;
  var switchPlace2 = 0;
  var sumAngle = 0; //sum of the angle between switches

  for(var j = 0; j < route.length; j++) {
    if (typeof route[j].switch !== 'undefined'){ //look for pieces with switch
      if (switchPlace1 < 0) { //skip first switch 
      	switchPlace1 = 0; switchPlace2 = j; continue;
      } 
      sumAngle = 0; 
      switchPlace0 = switchPlace1; //save first point of prev interval (switchPlace1, switchPlace2)
      switchPlace1 = switchPlace2; //determine first point for new interval 
      switchPlace2 = j; //determine second point for new interval

      for(var m = switchPlace1; m < switchPlace2; m++){ //count the sum of angle between intervals
        if (typeof route[m].angle !== 'undefined') sumAngle += route[m].angle;
      }

      if (sumAngle === 0) continue; //if no angle or changes, do nothing
      if (sumAngle > 0) switchTick.push({dir: "Right", pieceIndex: switchPlace0}) //if sumAngle is positiv switch on right
        else switchTick.push({dir: "Left", pieceIndex: switchPlace0}) //if sumAngle is negativ switch on left
    }
  }
}

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  logs += "S: " + JSON.stringify(json) + "\n"; //saving Sent data

  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  logs += "R: " + JSON.stringify(data) + "\n"; //saving Recieved data
  if (tick % 50 == 0) console.log("Game tick: " + tick); //view on wich tick is the game every 50 ticks
  tick++; //increase tick each time bot recieves data

/* 
	CAR MANIPULATION 
	Here is the place were you recieve car position and state on road,
	and send back responses to server
*/
  if (data.msgType === 'carPositions') {
	if (once) {once = false; return;} //first carPosition is befor the Start 

	// send the signal of switch lane based on "switchTick" array
	if( data.data[0].piecePosition.pieceIndex == switchTick[nextSwitchPointer].pieceIndex) {
	  sendSwitchLane(switchTick[nextSwitchPointer].dir); 

	  if((nextSwitchPointer + 1) < switchTick.length) nextSwitchPointer++ // move to next switch
	  else {nextSwitchPointer = 0;} // if it's the last switch, point from the begining for next lap
	}

	// send the throttle
	sendThrottle(0.6);
  } 
/* Finish of CAR MANIPULATION */



  else {
  	if (data.msgType === 'gameInit') { //game initialization data
  	  
  	  //rules after which car should switch the lane
  	  createSwitchTick(data.data.race.track.pieces);

  	} else if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');

      storeInfo("./tmp/logSentRecieve", logs); //save logs in a file
    } 

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
